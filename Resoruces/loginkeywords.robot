*** Settings ***
Library     SeleniumLibrary
Variables  ../Pageobjects/Locators.py

*** Keywords ***
Launch browser
    [Arguments]  ${url}     ${browser}
    open browser    ${url}   ${browser}
    maximize browser window

Enter username
    [Arguments]  ${username}
    Input Text  ${txt_loginusername}    ${username}

Enter password
    [Arguments]  ${password}
    Input Text  ${txt_loginpassword}    ${password}

Click signin
    click button    ${btn_signin}

Verify successfull login
    title should be     Find a Flight: Mercury Tours:

Close the browsers
    close all browsers
